/*
 @utils.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const fs = require('fs');
const constants = require('./../constants/constants');
const CompilersServiceError = require('./../errors/compilers_service_error');
const CompilersServiceErrorFileSystem = require('./../errors/compilers_service_error_file_system');
const OBJECT_TYPE = 'object';
const STRING_TYPE = 'string';
const NULL_PARAMETERS_MESSAGE = "Function doesn't accept null parameters";
const NULL_PARAMETER_MESSAGE = "Function doesn't accept null parameter";
const OBJECT_NEEDED = "Function needs a objects as parameters";
const STRING_NEEDED = "File path should be String";


class Utils {
    
    // Creates a folder
    create_folder(path_folder) {
        this.validate_path(path_folder);
        fs.mkdirSync(path_folder);
    }

    // Verifies if exists a path 
    path_exist(path_folder) {
        this.validate_path(path_folder);
        return fs.existsSync(path_folder);
    }

    // Deletes a path
    delete_path(path_folder) {
        this.validate_path(path_folder);
        fs.rmdirSync(path_folder, { recursive: true });
    }

    // Creates file  
    create_file(file) {
        this.validate_path(file.path);
        fs.writeFileSync(file.path, file.content);
    }

    // Gets file path and content 
    get_data_file(project, file) {
        this.validate_parameters(project, file);
        let file_path = this.get_file_path(project, file);
        let content;
        if (file.file_name == 'main'){
            content = constants.languages[project.language].cabecera + 'package ' + project.name + 
                constants.languages[project.language].main_content;
        }
        else {
            content = file.content;
        }
        return {
            path: file_path,
            content: content
        };
    }

    // Gets file path 
    get_file_path(project, file) {
        this.validate_parameters(project, file);
        return constants.projects_path + '\\' + project.name + '\\' + file.file_name + 
            constants.languages[project.language].type;
    }

    // Validates parameters of function 
    validate_parameters(project, file) {
        if (project == null || file == null) {
            throw new CompilersServiceError(NULL_PARAMETERS_MESSAGE);
        }
        if (typeof(project) != OBJECT_TYPE || typeof(file) != OBJECT_TYPE) {
            throw new CompilersServiceError(OBJECT_NEEDED);
        }
    }

    // Validates path to use fs library 
    validate_path(path) {
        if (path == null) {
            throw new CompilersServiceErrorFileSystem(NULL_PARAMETER_MESSAGE);
        }
        if (typeof(path) != STRING_TYPE) {
            throw new CompilersServiceErrorFileSystem(STRING_NEEDED);
        }
    }
}

// Exports class
module.exports = Utils;
