/*
@ide_jala.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


class Results{

    //private variables
    #pid;
    #result;

    // defines the constructor for its children
    constructor(pid, result) {
        this.#pid = pid;
        this.#result = result;
    }

    // returns the PID
    get_pid(){
        return this.#pid
    }
        
    // returns the console results
    get_result(){
        return this.#result
    }
}


module.exports = Results;
//var r = new Results(2, 'asdcds')
//console.log(r.get_pid())