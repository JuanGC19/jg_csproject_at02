/*
 @exists_language_validation_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const expect = require("chai").expect;
const LanguageValidation = require('../../../../common/validation_strategy/language_exists_validation');
const CompilersServiceError = require('../../../../common/errors/compilers_service_error');


describe('language exist validation test', () => {

    it("Send wrong language ", () => {
        let language_validation = new LanguageValidation('javaI');
        expect(() => { language_validation.validate(); }).to.throw(CompilersServiceError, 'this language does not exist');
    });
});
