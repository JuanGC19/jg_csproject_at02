/*
@parameters_test.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const expect = require("chai").expect;
const Parameters = require('../../../../../core/compiler/command_factory/parameters');
const CompilersServiceError = require('../../../../../common/errors/compilers_service_error');


describe('parameters test', () => {

    it("build parameters with null values", () => {
        expect(() => { new Parameters(null , null, null); })
        .to.throw(CompilersServiceError, 'binary path in null or empty');
    });

    it("build parameters with empty values", () => {
        expect(() => { new Parameters("" , "", ""); })
        .to.throw(CompilersServiceError, 'binary path in null or empty');
    });
});
