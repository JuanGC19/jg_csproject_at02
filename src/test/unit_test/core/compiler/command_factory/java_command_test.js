/*
@java_command_test.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const expect = require("chai").expect;
const JavaCommand = require('../../../../../core/compiler/command_factory/java_command');
const Parameters = require('../../../../../core/compiler/command_factory/parameters');
const CompilersServiceError = require('../../../../../common/errors/compilers_service_error');
const constants = require('../../../../../common/constants/constants');


describe('java command test', () => {

    it("build java command on null ",() => {
        let command = new JavaCommand();
        expect(() => { command.builder(null); })
        .to.throw(CompilersServiceError, 'command parameters in null or empty');
    });

    it("build java command on empty", () => {
        let command = new JavaCommand();
        expect(() => { command.builder(""); })
        .to.throw(CompilersServiceError, 'command parameters in null or empty');
    });

    it("build java command on an empty array", () => {
        let command = new JavaCommand();
        expect(() => { command.builder([]); })
        .to.throw(CompilersServiceError, 'command parameters in null or empty');
    });

    it("build java command with null parameters", () => {
        expect(() => { 
            let parameters = new Parameters(null , null, null);
            let command = new JavaCommand();
            command.builder(parameters); })
        .to.throw(CompilersServiceError, 'binary path in null or empty');
    });

    it("build java command", async() => {
        let parameters = new Parameters(constants.languages['java'].path, constants.projects_path, "projectName");
        let command = new JavaCommand();
        command = await command.builder(parameters);
        expect(command).to.be.an('array');
    });
});
