/*
 @console_controller_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const expect = require("chai").expect;
const request = require('supertest');
const assert = require('assert');
const app = require('../../../app');
const conn = require('../../../index');
const project_id = '60521eac923f4d4a447de6a4';


describe('console controller test', () => {

    // Negative tests from console controller to run a project
    it("get results with a incorrect id", (done) => {
        request(app).get('/api/v1/console/project/60478de')
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('error');
            assert.deepStrictEqual(body['error'], "Project doesn't exist in database");
            done();
        }).catch((err) =>{});
    });

    it("get status with a incorrect id", (done) => {
        request(app).get('/api/v1/console/project/60478de')
        .then((res) => {
            const status = res.status;
            assert.deepStrictEqual(status, 400);
            done();
        }).catch((err) =>{});
    });

    // Positive tests from console controller to run a project
    it("get status with a correct id", (done) => {
        request(app).get('/api/v1/console/project/'+project_id)
        .then((res) => {
            const status = res.status;
            assert.deepStrictEqual(status, 200);
            done();
        }).catch((err) =>{});
    });

    it("get result with a correct id", (done) => {
        request(app).get('/api/v1/console/project/'+project_id)
        .then((res) => {
            const body = res.body;
            expect(body).to.contain.property('pid');
            expect(body).to.contain.property('console');
            done();
        }).catch((err) =>{});
    });
});
