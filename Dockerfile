FROM node AS build
COPY . /app
WORKDIR /app/src
RUN mv .env.example .env && \
    npm install && \
    npm run test:unit

FROM node
WORKDIR /app
COPY --from=build /app/src .
EXPOSE 8000
ENTRYPOINT ["npm","start"]
